/*******************************************************************
          Copyright (C) 1986 by Manuel E. Bermudez
          Translated to C - 1991
 ********************************************************************/

#include <stdio.h>
#include <header/CommandLine.h>
#include <header/Open_File.h>
#include <header/Table.h>
#include <header/Text.h>
#include <header/Error.h>
#include <header/String_Input.h>
#include <header/Tree.h>
#include <header/Code.h>
#include <header/CodeGenerator.h>
#define LeftMode 0
#define RightMode 1

/*  ABSTRACT MACHINE OPERATIONS  */
#define    NOP          1   /* 'NOP'       */
#define    HALTOP       2   /* 'HALT'      */
#define    LITOP        3   /* 'LIT'       */
#define    LLVOP        4   /* 'LLV'       */
#define    LGVOP        5   /* 'LGV'       */
#define    SLVOP        6   /* 'SLV'       */
#define    SGVOP        7   /* 'SGV'       */
#define    LLAOP        8   /* 'LLA'       */
#define    LGAOP        9   /* 'LGA'       */
#define    UOPOP       10   /* 'UOP'       */
#define    BOPOP       11   /* 'BOP'       */
#define    POPOP       12   /* 'POP'       */
#define    DUPOP       13   /* 'DUP'       */
#define    SWAPOP      14   /* 'SWAP'      */
#define    CALLOP      15   /* 'CALL'      */
#define    RTNOP       16   /* 'RTN'       */
#define    GOTOOP      17   /* 'GOTO'      */
#define    CONDOP      18   /* 'COND'      */
#define    CODEOP      19   /* 'CODE'      */
#define    SOSOP       20   /* 'SOS'       */
#define    LIMITOP     21   /* 'LIMIT'     */

/* ABSTRACT MACHINE OPERANDS */

/* UNARY OPERANDS */
#define    UNOT        22   /* 'UNOT'     */
#define    UNEG        23   /* 'UNEG'     */
#define    USUCC       24   /* 'USUCC'    */
#define    UPRED       25   /* 'UPRED'    */
/* BINARY OPERANDS */
#define    BAND        26   /* 'BAND'     */
#define    BOR         27   /* 'BOR'      */
#define    BPLUS       28   /* 'BPLUS'    */
#define    BMINUS      29   /* 'BMINUS'   */
#define    BMULT       30   /* 'BMULT'    */
#define    BDIV        31   /* 'BDIV'     */
#define    BEXP        32   /* 'BEXP'     */
#define    BMOD        33   /* 'BMOD'     */
#define    BEQ         34   /* 'BEQ'      */
#define    BNE         35   /* 'BNE'      */
#define    BLE         36   /* 'BLE'      */
#define    BGE         37   /* 'BGE'      */
#define    BLT         38   /* 'BLT'      */
#define    BGT         39   /* 'BGT'      */
/* OS SERVICE CALL OPERANDS */
#define    TRACEX      40   /* 'TRACEX'   */
#define    DUMPMEM     41   /* 'DUMPMEM'  */
#define    OSINPUT     42   /* 'INPUT'    */
#define    OSINPUTC    43   /* 'INPUT'    */
#define    OSOUTPUT    44   /* 'OUTPUT'   */
#define    OSOUTPUTC   45   /* 'OUTPUT'   */
#define    OSOUTPUTL   46   /* 'OUTPUTL'  */
#define    OSEOF       47   /* 'EOF'      */

/* TREE NODE NAMES */
#define ProgramNode 48

#define SubProgsNode 49
#define FunctionNode 50
#define ProcedureNode 51
#define ParamsNode 52
#define CallNode 53
#define ReturnNode 54

#define ConstsNode 55
#define ConstNode 56
#define TypesNode 57
#define TypeNode 58
#define LitNode 59
#define DclnsNode 60
#define DclnNode 61
#define BlockNode 62

#define AssignNode 63
#define SwapNode 64
#define OutputNode 65
#define ReadNode 66
#define SuccNode 67
#define PredNode 68
#define OrdNode 69
#define ChrNode 70
#define IfNode 71
#define WhileNode 72
#define RepeatNode 73
#define LoopNode 74
#define CaseNode 75
#define UptoNode 76
#define DowntoNode 77
#define ExitNode 78
#define NullNode 79

#define CaseItemNode 80
#define DDNode 81
#define OtherwiseNode 82

#define LENode 83
#define GENode 84
#define EqNode 85
#define NEqNode 86
#define GTNode 87
#define LTNode 88

#define PlusNode 89
#define MinusNode 90
#define OrNode 91

#define StarNode 92
#define DivNode 93
#define AndNode 94
#define ModNode 95

#define NotNode 96
#define ExpNode 97

#define EOFNode 98
#define StringNode 99
#define IntegerNode 100
#define CharacterNode 101

#define IdentifierNode 102

#define LOOP_CTXT 103
#define FOR_CTXT 104
#define SUBPROG_CTXT 105

#define IntegerTNode 106
#define BooleanTNode 107
#define CharacterTNode 108

#define TrueNode 109
#define FalseNode 110

#define NumberOfNodes 110

typedef int Mode;

FILE *CodeFile;
char *CodeFileName;
Clabel HaltLabel;

char *mach_op[] =
{"NOP","HALT","LIT","LLV","LGV","SLV","SGV","LLA","LGA",
 "UOP","BOP","POP","DUP","SWAP","CALL","RTN","GOTO","COND",
 "CODE","SOS","LIMIT","UNOT","UNEG","USUCC","UPRED","BAND",
 "BOR","BPLUS","BMINUS","BMULT","BDIV","BEXP","BMOD","BEQ",
 "BNE","BLE","BGE","BLT","BGT","TRACEX","DUMPMEM","INPUT",
 "INPUTC","OUTPUT","OUTPUTC","OUTPUTL","EOF"};

/******************************************************************
   add new node names to the end of the array, keeping in strict order
   as defined above, then adjust the j loop control variable in
   InitializeNodeNames().
 *******************************************************************/
char *node_name[] = {
        "program",

        "subprogs",
        "function",
        "procedure",
        "params",
        "call",
        "return",

        "consts",
        "const",
        "types",
        "type",
        "lit",
        "dclns",
        "dcln",
        "block",

        "assign",
        "swap",
        "output",
        "read",
        "succ",
        "pred",
        "ord",
        "chr",
        "if",
        "while",
        "repeat",
        "loop",
        "case",
        "upto",
        "downto",
        "exit",
        "<null>",

        "caseitem",
        "..",
        "otherwise",

        "<=",
        ">=",
        "=",
        "<>",
        ">",
        "<",

        "+",
        "-",
        "or",

        "*",
        "/",
        "and",
        "mod",

        "not",
        "**",

        "eof",
        "<string>",
        "<integer>",
        "<char>",

        "<identifier>",

        "<loop_ctxt>",
        "<for_ctxt>",
        "<subprog_ctxt>",

        "integer",
        "boolean",
        "char",

        "true",
        "false"
};


void CodeGenerate(int argc, char *argv[])
{
        int NumberTrees;

        InitializeCodeGenerator(argc,argv);
        Tree_File = Open_File("_TREE", "r");
        NumberTrees = Read_Trees();
        fclose (Tree_File);

        HaltLabel = ProcessNode (RootOfTree(1), NoLabel);
        CodeGen0 (HALTOP, HaltLabel);

        CodeFile = Open_File (CodeFileName, "w");
        DumpCode (CodeFile);
        fclose(CodeFile);

        if (TraceSpecified)
                fclose (TraceFile);

/******************************************************************
   enable this code to write out the tree after the code generator
   has run.  It will show the new decorations made with MakeAddress().
 *******************************************************************/


        Tree_File = fopen("_TREE", "w");
        Write_Trees();
        fclose (Tree_File);
}


void InitializeCodeGenerator(int argc,char *argv[])
{
        InitializeMachineOperations();
        InitializeNodeNames();
        FrameSize = 0;
        CurrentProcLevel = 0;
        LabelCount = 0;
        CodeFileName = System_Argument("-code", "_CODE", argc, argv);
}


void InitializeMachineOperations(void)
{
        int i,j;

        for (i=0, j=1; i < 47; i++, j++)
                String_Array_To_String_Constant (mach_op[i],j);
}



void InitializeNodeNames(void)
{
        int i,j;

        for (i=0, j=48; j <= NumberOfNodes; i++, j++)
                String_Array_To_String_Constant (node_name[i],j);
}



String MakeStringOf(int Number)
{
        Stack Temp;

        Temp = AllocateStack (50);
        ResetBufferInTextTable();
        if (Number == 0)
                AdvanceOnCharacter ('0');
        else
        {
                while (Number > 0)
                {
                        Push (Temp,(Number % 10) + 48);
                        Number /= 10;
                }

                while ( !(IsEmpty (Temp)))
                        AdvanceOnCharacter ((char)(Pop(Temp)));
        }
        return (ConvertStringInBuffer());
}



void Reference(TreeNode T, Mode M, Clabel L)
{
        String Op;
        int Addr = Decoration(Decoration(T));

        switch (M)
        {
        case LeftMode:
                DecrementFrameSize();
                if (ProcLevel (Addr) == 0)
                        Op = SGVOP;
                else
                        Op = SLVOP;
                CodeGen1 (Op,MakeStringOf(FrameDisplacement (Addr)),L);
                break;
        case RightMode:
                if(NodeName(Decoration(Child(Decoration(T), 1))) == DclnNode){
                        IncrementFrameSize();
                        if (ProcLevel (Addr) == 0)
                                Op = LGVOP;
                        else
                                Op = LLVOP;
                        CodeGen1 (Op,MakeStringOf(FrameDisplacement (Addr)),L);
                } else {
                        CodeGen1(LITOP, Addr, L);
                        IncrementFrameSize();
                }
                break;
        }
}


void Expression (TreeNode T, Clabel CurrLabel)
{
        if (TraceSpecified)
        {
                fprintf (TraceFile, "<<< CODE GENERATOR >>> Processing Node ");
                Write_String (TraceFile, NodeName (T) );
                fprintf (TraceFile, " , Label is  ");
                Write_String (TraceFile, CurrLabel);
                fprintf (TraceFile, "\n");
        }

        switch (NodeName(T))
        {
        case LENode:
        case PlusNode:
        case GENode:
        case EqNode:
        case NEqNode:
        case GTNode:
        case LTNode:
        case OrNode:
        case StarNode:
        case DivNode:
        case AndNode:
        case ModNode:
        case ExpNode:
                Expression ( Child(T,1), CurrLabel);
                Expression ( Child(T,2), NoLabel);
                if      (NodeName(T) == LENode) CodeGen1 (BOPOP, BLE, NoLabel);
                else if (NodeName(T) == PlusNode) CodeGen1 (BOPOP, BPLUS, NoLabel);
                else if (NodeName(T) == GENode) CodeGen1 (BOPOP, BGE, NoLabel);
                else if (NodeName(T) == EqNode) CodeGen1 (BOPOP, BEQ, NoLabel);
                else if (NodeName(T) == NEqNode) CodeGen1 (BOPOP, BNE, NoLabel);
                else if (NodeName(T) == GTNode) CodeGen1 (BOPOP, BGT, NoLabel);
                else if (NodeName(T) == LTNode) CodeGen1 (BOPOP, BLT, NoLabel);
                else if (NodeName(T) == OrNode) CodeGen1 (BOPOP, BOR, NoLabel);
                else if (NodeName(T) == StarNode) CodeGen1 (BOPOP, BMULT, NoLabel);
                else if (NodeName(T) == DivNode) CodeGen1 (BOPOP, BDIV, NoLabel);
                else if (NodeName(T) == AndNode) CodeGen1 (BOPOP, BAND, NoLabel);
                else if (NodeName(T) == ModNode) CodeGen1 (BOPOP, BMOD, NoLabel);
                else if (NodeName(T) == ExpNode) CodeGen1 (BOPOP, BEXP, NoLabel);
                DecrementFrameSize();
                break;

        case MinusNode:
                Expression ( Child(T,1), CurrLabel);
                if (Rank(T) == 2)
                {
                        Expression ( Child(T,2), NoLabel);
                        CodeGen1 (BOPOP, BMINUS, NoLabel);
                        DecrementFrameSize();
                }
                else
                        CodeGen1 (UOPOP, UNEG, NoLabel);
                break;

        case NotNode:
                Expression ( Child(T,1), CurrLabel);
                CodeGen1 (UOPOP, UNOT, NoLabel);
                break;

        case IntegerNode:
                CodeGen1 (LITOP, NodeName (Child(T,1)), CurrLabel);
                IncrementFrameSize();
                break;

        case IdentifierNode:
                Reference (T,RightMode,CurrLabel);
                break;

        case EOFNode:
                CodeGen1(SOSOP, OSEOF, CurrLabel);
                IncrementFrameSize();
                break;

        case CharacterNode:
                CodeGen1 (LITOP, MakeStringOf(Character(NodeName(Child(T,1)),2)), CurrLabel);
                IncrementFrameSize();
                break;

        case SuccNode:
        case PredNode:
                Expression(Child(T, 1), CurrLabel);
                NodeName(T) == SuccNode ? CodeGen1 (UOPOP, USUCC, NoLabel) : CodeGen1 (UOPOP, UPRED, NoLabel);
                if(Rank(Decoration(T)) > 1 && NodeName(Child(Decoration(T), 2)) == LitNode){
                        CodeGen1 (LITOP, MakeStringOf(0), NoLabel);
                        IncrementFrameSize();
                        CodeGen1 (LITOP, MakeStringOf(Rank(Child(Decoration(T), 2)) - 1), NoLabel);
                        IncrementFrameSize();
                        CodeGen0 (LIMITOP, NoLabel);
                        DecrementFrameSize();
                        DecrementFrameSize();
                }
                break;

        case OrdNode:
        case ChrNode:
                Expression(Child(T, 1), CurrLabel);
                break;

        case CallNode:
                CodeGen1(LITOP, MakeStringOf(0), CurrLabel);
                IncrementFrameSize();
                int Kid;
                for(Kid = 2 ;Kid <= Rank(T); Kid++)
                        Expression(Child(T, Kid), NoLabel);
                CodeGen1(CODEOP, Decoration(Decoration(Child( Decoration(Child(T,1)),1))), NoLabel);
                for(Kid = 2 ;Kid <= Rank(T); Kid++)
                        DecrementFrameSize();
                CodeGen1(CALLOP, MakeStringOf(FrameSize-1), NoLabel);
                break;

        default:
                ReportTreeErrorAt(T);
                printf ("<<< CODE GENERATOR >>> : UNKNOWN NODE NAME ");
                Write_String (stdout,NodeName(T));
                printf ("\n");

        } /* end switch */
} /* end Expression */



Clabel ProcessNode (TreeNode T, Clabel CurrLabel)
{
        int Kid, Num;
        Clabel Label1, Label2, Label3, Label4;

        if (TraceSpecified)
        {
                fprintf (TraceFile, "<<< CODE GENERATOR >>> Processing Node ");
                Write_String (TraceFile, NodeName (T) );
                fprintf (TraceFile, " , Label is  ");
                Write_String (TraceFile, CurrLabel);
                fprintf (TraceFile, "\n");
        }

        switch (NodeName(T))
        {
        case ProgramNode:
                for (Kid = 2; Kid <= 5; Kid++)
                        CurrLabel = ProcessNode (Child(T,Kid), CurrLabel);
                Label1 = MakeLabel();
                CodeGen1 (GOTOOP, Label1, CurrLabel);
                ProcessNode(Child(T, 6), NoLabel);
                CurrLabel = ProcessNode(Child(T, 7), Label1);
                return (CurrLabel);

        case SubProgsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid), CurrLabel);
                break;

        case FunctionNode:
            	OpenFrame();
            	Decorate(Child(T,1), MakeAddress());
            	IncrementFrameSize();
            	Label1 = MakeLabel();
            	Decorate(T, Label1);
            	ProcessNode(Child(T,2), CurrLabel);
                CurrLabel = ProcessNode(Child(T,4), CurrLabel);
                ProcessNode(Child(T,5), CurrLabel);
            	CurrLabel = ProcessNode(Child(T,6), Label1);
            	CurrLabel = ProcessNode(Child(T,7), CurrLabel);
            	CodeGen1(LLVOP, MakeStringOf(0), CurrLabel);
            	CodeGen1(RTNOP, MakeStringOf(1), NoLabel);
            	CloseFrame();
            	return (NoLabel);

        case ProcedureNode:
            	OpenFrame();
            	Decorate(Child(T,1), MakeAddress());
            	IncrementFrameSize();
            	Label1 = MakeLabel();
            	Decorate(T, Label1);
            	ProcessNode(Child(T,2), CurrLabel);
                CurrLabel = ProcessNode(Child(T,3), CurrLabel);
                ProcessNode(Child(T,4), CurrLabel);
            	CurrLabel = ProcessNode(Child(T,5), Label1);
            	CurrLabel = ProcessNode(Child(T,6), CurrLabel);
            	CodeGen1(LLVOP, MakeStringOf(0), CurrLabel);
            	CodeGen1(RTNOP, MakeStringOf(0), NoLabel);
            	CloseFrame();
            	return (NoLabel);

        case ParamsNode:
                for(Num = 1; Num <= Rank(T); Num++){
                        for (Kid = 1; Kid < Rank(Child(T,Num)); Kid++){
                                int addr = MakeAddress();
                                Decorate ( Child(Child(T, Num), Kid), addr);
                                IncrementFrameSize();
                        }
                }
                return (NoLabel);


        case ReturnNode:
                if(Rank(T) > 0)
                    Expression(Child(T,1), CurrLabel);
                CodeGen1(RTNOP, MakeStringOf(1), Rank(T)?NoLabel:CurrLabel);
                DecrementFrameSize();
                return (NoLabel);

        case CallNode:
                CodeGen1(LITOP, MakeStringOf(0), CurrLabel);
                IncrementFrameSize();
                for(Kid = 2 ;Kid <= Rank(T); Kid++)
                Expression(Child(T, Kid), NoLabel);
                CodeGen1(CODEOP, Decoration(Decoration(Child(Decoration(Child(T,1)),1))), NoLabel);
                for(Kid = 2 ;Kid <= Rank(T); Kid++)
                DecrementFrameSize();
                CodeGen1(CALLOP, MakeStringOf(FrameSize-1), NoLabel);
                DecrementFrameSize();
                return (NoLabel);

        case TypesNode:
        case ConstsNode:
        case DclnsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        CurrLabel = ProcessNode (Child(T,Kid), CurrLabel);
                return (CurrLabel);

        case TypeNode:
                if (Rank(T) == 2 && NodeName(Child(T, 2)) == LitNode)
                        CurrLabel = ProcessNode(Child(T, 2), CurrLabel);
                return (CurrLabel);

        case LitNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        Decorate(Child(T, Kid), MakeStringOf(Kid - 1));
                return (CurrLabel);

        case ConstNode:
                switch(NodeName(Child(T, 2))){
                    case IntegerNode:
                        Kid = NodeName(Child(Child(T, 2), 1));
                        break;
                    case CharacterNode:
                        Kid = MakeStringOf(Character(NodeName(Child(Child(T, 2), 1)), 2));
                        break;
                    case IdentifierNode:
                        Kid = Decoration(Decoration(Child(T, 2)));
                        break;
                }
                Decorate(Child(T, 2), Child(Decoration(Child(T, 1)), 1));
                Decorate(Child(T, 1), Kid);
                return (CurrLabel);

        case DclnNode:
                for (Kid = 1; Kid < Rank(T); Kid++)
                {
                        if (Kid == 1)
                                CodeGen1 (LITOP,MakeStringOf(0),CurrLabel);
                        else
                                CodeGen1 (LITOP,MakeStringOf(0),NoLabel);
                        Num = MakeAddress();
                        Decorate ( Child(T,Kid), Num);
                        IncrementFrameSize();
                }
                return (NoLabel);

        case BlockNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        CurrLabel = ProcessNode (Child(T,Kid), CurrLabel);
                return (CurrLabel);


        case AssignNode:
                Expression (Child(T,2), CurrLabel);
                Reference (Child(T,1), LeftMode, NoLabel);
                return (NoLabel);

        case SwapNode:
                Reference(Child(T, 1), RightMode, CurrLabel);
                Reference(Child(T, 2), RightMode, NoLabel);
                CodeGen0(SWAPOP, NoLabel);
                Reference(Child(T, 2), LeftMode, NoLabel);
                Reference(Child(T, 1), LeftMode, NoLabel);
                return (NoLabel);


        case OutputNode: /* TODO: fix this stuff */
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        if(NodeName(Child(T, Kid)) == StringNode){
                                Num = 2;
                                while(Character(NodeName(Child(Child(T, Kid), 1)), Num) != '"'){
                                    if(Character(NodeName(Child(Child(T, Kid), 1)), Num) == '\\'){
                                            Num++;
                                            switch(Character(NodeName(Child(Child(T, Kid), 1)), Num)){
                                                    case 'a':   CodeGen1 (LITOP, MakeStringOf('\a'), CurrLabel); break;
                                                    case 'b':   CodeGen1 (LITOP, MakeStringOf('\b'), CurrLabel); break;
                                                    case 'f':   CodeGen1 (LITOP, MakeStringOf('\f'), CurrLabel); break;
                                                    case 'n':   CodeGen1 (LITOP, MakeStringOf('\n'), CurrLabel); break;
                                                    case 'r':   CodeGen1 (LITOP, MakeStringOf('\r'), CurrLabel); break;
                                                    case 't':   CodeGen1 (LITOP, MakeStringOf('\t'), CurrLabel); break;
                                                    case 'v':   CodeGen1 (LITOP, MakeStringOf('\v'), CurrLabel); break;
                                                    case '\\':  CodeGen1 (LITOP, MakeStringOf('\\'), CurrLabel); break;
                                                    case '\'':  CodeGen1 (LITOP, MakeStringOf('\''), CurrLabel); break;
                                                    case '"':   CodeGen1 (LITOP, MakeStringOf('"'), CurrLabel); break;
                                                    case '?':   CodeGen1 (LITOP, MakeStringOf('?'), CurrLabel); break;
                                                    default:   CodeGen1 (LITOP, MakeStringOf('?'), CurrLabel); break;
                                            }
                                    }
                                    else
                                            CodeGen1 (LITOP, MakeStringOf(Character(NodeName(Child(Child(T, Kid), 1)), Num)), CurrLabel);
                                    IncrementFrameSize();
                                    CodeGen1 (SOSOP, OSOUTPUTC, NoLabel);
                                    DecrementFrameSize();
                                    Num++;
                                    CurrLabel = NoLabel;
                                }
                        }
                        else if(NodeName(Child(T, Kid)) == IdentifierNode && NodeName(Decoration(Child(Decoration(Child(T, Kid)), 1))) != LitNode)
                        {
                                Expression (Child(T,Kid), CurrLabel);

                                if(Decoration(Decoration(Child(Decoration(Child(Decoration(Child(T, Kid)), 1)), Rank(Decoration(Child(Decoration(Child(T, Kid)), 1)))))) == Child (Child (RootOfTree(1), 2), 3))
                                        CodeGen1 (SOSOP, OSOUTPUTC, NoLabel);
                                else
                                        CodeGen1 (SOSOP, OSOUTPUT, NoLabel);
                                DecrementFrameSize();
                                CurrLabel = NoLabel;
                        }
                        else if(Decoration(Child(T, Kid)) == Child (Child (RootOfTree(1), 2), 3) || NodeName(Child(T, Kid)) == CharacterNode)
                        {
                                Expression (Child(T,Kid), CurrLabel);
                                CodeGen1 (SOSOP, OSOUTPUTC, NoLabel);
                                DecrementFrameSize();
                                CurrLabel = NoLabel;
                        }
                        else
                        {
                                Expression (Child(T,Kid), CurrLabel);
                                CodeGen1 (SOSOP, OSOUTPUT, NoLabel);
                                DecrementFrameSize();
                                CurrLabel = NoLabel;
                        }

                CodeGen1 (SOSOP, OSOUTPUTL, NoLabel);
                return (NoLabel);

        case ReadNode:
                for (Kid = 1; Kid <= Rank(T); Kid++){
                        if(Decoration(Decoration(Child(Decoration(Child(Decoration(Child(T, Kid)), 1)), Rank(Decoration(Child(Decoration(Child(T, Kid)), 1)))))) == Child (Child (RootOfTree(1), 2), 3))
                                CodeGen1 (SOSOP, OSINPUTC, CurrLabel);
                        else
                                CodeGen1 (SOSOP, OSINPUT, CurrLabel);
                        IncrementFrameSize();
                        Reference(Child(T, Kid), LeftMode, NoLabel);
                        CurrLabel = NoLabel;
                }
                return CurrLabel;

        case IfNode:
                Expression (Child(T,1), CurrLabel);
                Label1 = MakeLabel();
                Label2 = MakeLabel();
                Label3 = MakeLabel();
                CodeGen2 (CONDOP,Label1,Label2, NoLabel);
                DecrementFrameSize();
                CodeGen1 (GOTOOP, Label3, ProcessNode (Child(T,2), Label1) );
                if (Rank(T) == 3)
                        CodeGen0 (NOP, ProcessNode (Child(T,3),Label2));
                else
                        CodeGen0 (NOP, Label2);
                return (Label3);


        case WhileNode:
                if (CurrLabel == NoLabel)
                        Label1 = MakeLabel();
                else
                        Label1 = CurrLabel;
                Label2 = MakeLabel();
                Label3 = MakeLabel();
                Expression (Child(T,1), Label1);
                CodeGen2 (CONDOP, Label2, Label3, NoLabel);
                DecrementFrameSize();
                CodeGen1 (GOTOOP, Label1, ProcessNode (Child(T,2), Label2) );
                return (Label3);

        case RepeatNode:
                if(CurrLabel == NoLabel) {
                        Label1 = MakeLabel();
                        CurrLabel = Label1;
                }
                else
                        Label1 = CurrLabel;
                for (Kid = 1; Kid <= Rank(T) - 1; Kid++)
                        CurrLabel = ProcessNode (Child(T,Kid), CurrLabel);
                Expression (Child(T, Rank(T)), CurrLabel);
                Label2 = MakeLabel();
                CodeGen2 (CONDOP, Label2, Label1, NoLabel);
                DecrementFrameSize();
                return (Label2);

        case LoopNode:
                if(CurrLabel == NoLabel) {
                        Label1 = MakeLabel();
                        CurrLabel = Label1;
                }
                else
                        Label1 = CurrLabel;
                Label2 = Decoration(T) ? MakeLabel() : NoLabel;
                Decorate(T, Label2);
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        CurrLabel = ProcessNode (Child(T,Kid), CurrLabel);
                CodeGen1 (GOTOOP, Label1, CurrLabel);
                return (Label2);

        case ExitNode:
                CodeGen1(GOTOOP, Decoration(Decoration(T)), CurrLabel);
                return (NoLabel);

        case CaseNode:
                /* exit label */
                Label1 = MakeLabel();
                Decorate(T, Label1);

                Expression(Child(T, 1), CurrLabel);
                Label2 = NoLabel;
                for (Kid = 2; Kid <= Rank(T); Kid++) {
                        Decorate(Child(T,Kid), T);
                        Label2 = ProcessNode (Child(T,Kid), Label2);
                }

                if(NodeName(Child(T, Rank(T))) != OtherwiseNode) {
                        CodeGen1(POPOP, MakeStringOf(1), Label2);
                        DecrementFrameSize();
                } else
                        CodeGen0(NOP, Label2);

                return (Label1);

        case CaseItemNode:

                if(NodeName(Child(T, 1)) != DDNode) {
                        CodeGen0(DUPOP, CurrLabel);
                        IncrementFrameSize();
                        Expression(Child(T, 1), NoLabel);
                        CodeGen1(BOPOP, BEQ, NoLabel);
                        DecrementFrameSize();
                } else {
                        CodeGen0(DUPOP, CurrLabel);
                        IncrementFrameSize();
                        CodeGen0(DUPOP, NoLabel);
                        IncrementFrameSize();

                        Expression(Child(Child(T, 1), 1), NoLabel);
                        CodeGen1(BOPOP, BGE, NoLabel);
                        DecrementFrameSize();

                        CodeGen0(SWAPOP, NoLabel);

                        Expression(Child(Child(T, 1), 2), NoLabel);
                        CodeGen1(BOPOP, BLE, NoLabel);
                        DecrementFrameSize();

                        CodeGen1(BOPOP, BAND, NoLabel);
                        DecrementFrameSize();
                }

                Label1 = MakeLabel();

                Label3 = MakeLabel();
                CodeGen2 (CONDOP, Label1, Label3, NoLabel);
                DecrementFrameSize();

                CodeGen1(POPOP, MakeStringOf(1), Label1);
                DecrementFrameSize();

                Label2 = ProcessNode(Child(T,2), NoLabel);

                CodeGen1(GOTOOP, Decoration(Decoration(T)), Label2);

                return (Label3);

        case OtherwiseNode:
                CodeGen1 (LITOP, MakeStringOf(1), CurrLabel);
                IncrementFrameSize();

                Label1 = MakeLabel();

                Label3 = MakeLabel();
                CodeGen2 (CONDOP, Label1, Label3, NoLabel);
                DecrementFrameSize();

                CodeGen1(POPOP, MakeStringOf(1), Label1);
                DecrementFrameSize();

                Label2 = ProcessNode(Child(T,1), NoLabel);

                CodeGen1(GOTOOP, Decoration(Decoration(T)), Label2);

                return (Label3);

        case UptoNode:
        case DowntoNode:
                /* evaluate and store i */
                Expression (Child(T, 2), CurrLabel);
                Reference(Child(T, 1), LeftMode, NoLabel);
                /* evaluate f */
                Expression (Child(T, 3), NoLabel);

                /* duplicate f, load i and compare */
                Label1 = MakeLabel();
                CodeGen0(DUPOP, Label1);
                IncrementFrameSize();
                Reference(Child(T, 1), RightMode, NoLabel);
                NodeName(T) == DowntoNode ? CodeGen1 (BOPOP, BLE, NoLabel) : CodeGen1 (BOPOP, BGE, NoLabel);
                DecrementFrameSize();

                /* jump based on result 2 is success 3 is failure */
                Label2 = MakeLabel();
                Label3 = MakeLabel();
                CodeGen2 (CONDOP, Label2, Label3, NoLabel);
                DecrementFrameSize();

                /* process node 4 with success label */
                Label4 = ProcessNode(Child(T, 4), Label2);

                /* Load i, increment/decrement it, store the results in i */
                Reference(Child(T, 1), RightMode, Label4);
                NodeName(T) == DowntoNode ? CodeGen1 (UOPOP, UPRED, NoLabel) : CodeGen1 (UOPOP, USUCC, NoLabel);
                Reference(Child(T, 1), LeftMode, NoLabel);

                /* now go back to start of loop */
                CodeGen1(GOTOOP, Label1, NoLabel);

                /* pop Child 2 and reset i to 0*/
                CodeGen1(POPOP, MakeStringOf(1), Label3);
                DecrementFrameSize();
                CodeGen1(LITOP, MakeStringOf(0), NoLabel);
                IncrementFrameSize();
                Reference(Child(T, 1), LeftMode, NoLabel);

                return (NoLabel);

        case NullNode: return(CurrLabel);

        case DDNode:
        case StringNode:
        default:
                ReportTreeErrorAt(T);
                printf ("<<< CODE GENERATOR >>> : UNKNOWN NODE NAME ");
                Write_String (stdout,NodeName(T));
                printf ("\n");

        } /* end switch */
}   /* end ProcessNode */

/****************************************************************
              Copyright (C) 1986 by Manuel E. Bermudez
              Translated to C - 1991
 *****************************************************************/

#include <stdio.h>
#include <header/Open_File.h>
#include <header/CommandLine.h>
#include <header/Table.h>
#include <header/Text.h>
#include <header/Error.h>
#include <header/String_Input.h>
#include <header/Tree.h>
#include <header/Dcln.h>
#include <header/Constrainer.h>

#define ProgramNode     1

#define SubProgsNode    2
#define FunctionNode    3
#define ProcedureNode   4
#define ParamsNode      5
#define CallNode        6
#define ReturnNode      7

#define ConstsNode      8
#define ConstNode       9
#define TypesNode       10
#define TypeNode        11
#define LitNode         12
#define DclnsNode       13
#define DclnNode        14
#define BlockNode       15

#define AssignNode      16
#define SwapNode        17
#define OutputNode      18
#define ReadNode        19
#define SuccNode        20
#define PredNode        21
#define OrdNode         22
#define ChrNode         23
#define IfNode          24
#define WhileNode       25
#define RepeatNode      26
#define LoopNode        27
#define CaseNode        28
#define UptoNode        29
#define DowntoNode      30
#define ExitNode        31
#define NullNode        32

#define CaseItemNode    33
#define DDNode          34
#define OtherwiseNode   35

#define LENode          36
#define GENode          37
#define EqNode          38
#define NEqNode         39
#define GTNode          40
#define LTNode          41

#define PlusNode        42
#define MinusNode       43
#define OrNode          44

#define StarNode        45
#define DivNode         46
#define AndNode         47
#define ModNode         48

#define NotNode         49
#define ExpNode         50

#define EOFNode         51
#define StringNode      52
#define IntegerNode     53
#define CharacterNode   54

#define IdentifierNode  55

#define LOOP_CTXT       56
#define FOR_CTXT        57
#define SUBPROG_CTXT    58

#define IntegerTNode    59
#define BooleanTNode    60
#define CharacterTNode  61

#define TrueNode        62
#define FalseNode       63

#define NumberOfNodes   63


typedef TreeNode UserType;

/****************************************************************
   Add new node names to the end of the array, keeping in strict
   order with the define statements above, then adjust the i loop
   control variable in InitializeConstrainer().
 *****************************************************************/
char *node[] = {
        "program",

        "subprogs",
        "function",
        "procedure",
        "params",
        "call",
        "return",

        "consts",
        "const",
        "types",
        "type",
        "lit",
        "dclns",
        "dcln",
        "block",

        "assign",
        "swap",
        "output",
        "read",
        "succ",
        "pred",
        "ord",
        "chr",
        "if",
        "while",
        "repeat",
        "loop",
        "case",
        "upto",
        "downto",
        "exit",
        "<null>",

        "caseitem",
        "..",
        "otherwise",

        "<=",
        ">=",
        "=",
        "<>",
        ">",
        "<",

        "+",
        "-",
        "or",

        "*",
        "/",
        "and",
        "mod",

        "not",
        "**",

        "eof",
        "<string>",
        "<integer>",
        "<char>",

        "<identifier>",

        "<loop_ctxt>",
        "<for_ctxt>",
        "<subprog_ctxt>",

        "integer",
        "boolean",
        "char",

        "true",
        "false"
};


UserType TypeInteger, TypeBoolean, TypeCharacter;
boolean TraceSpecified;
FILE *TraceFile;
char *TraceFileName;
int NumberTreesRead, index;


void Constrain(void)
{
        int i;
        InitializeDeclarationTable();
        Tree_File = Open_File("_TREE", "r");
        NumberTreesRead = Read_Trees();
        fclose (Tree_File);

        AddIntrinsics();

#if 0
        printf("CURRENT TREE\n");
        for (i=1; i<=SizeOf(Tree); i++)
                printf("%2d: %d\n",i,Element(Tree,i));
#endif

        ProcessNode(RootOfTree(1));


        Tree_File = fopen("_TREE", "w");
        Write_Trees();
        fclose (Tree_File);

        if (TraceSpecified)
                fclose(TraceFile);
}


void InitializeConstrainer (int argc, char *argv[])
{
        int i, j;

        InitializeTextModule();
        InitializeTreeModule();

        for (i=0, j=1; i<NumberOfNodes; i++, j++)
                String_Array_To_String_Constant (node[i], j);

        index = System_Flag ("-trace", argc, argv);

        if (index)
        {
                TraceSpecified = true;
                TraceFileName = System_Argument ("-trace", "_TRACE", argc, argv);
                TraceFile = Open_File(TraceFileName, "w");
        }
        else
                TraceSpecified = false;
}


void AddIntrinsics (void)
{
        TreeNode TempTree;

        /* add types node */
        AddTree (TypesNode, RootOfTree(1), 2);

        /* add three type nodes */
        TempTree = Child (RootOfTree(1), 2);
        AddTree (TypeNode, TempTree, 1);
        TempTree = Child (RootOfTree(1), 2);
        AddTree (TypeNode, TempTree, 1);
        TempTree = Child (RootOfTree(1), 2);
        AddTree (TypeNode, TempTree, 1);

        /* build integer type node */
        TempTree = Child (Child (RootOfTree(1), 2), 1);
        AddTree (IdentifierNode, TempTree, 1);
        TempTree = Child (Child (Child (RootOfTree(1), 2), 1), 1);
        AddTree (IntegerTNode, TempTree, 1);

        /* build boolean type node */
        TempTree = Child (Child (RootOfTree(1), 2), 2);
        AddTree (IdentifierNode, TempTree, 1);
        TempTree = Child (Child (Child (RootOfTree(1), 2), 2), 1);
        AddTree (BooleanTNode, TempTree, 1);

        TempTree = Child (Child (RootOfTree(1), 2), 2);
        AddTree (LitNode, TempTree, 2);

        TempTree = Child (Child (Child (RootOfTree(1), 2), 2), 2);
        AddTree (IdentifierNode, TempTree, 1);
        TempTree = Child (Child (Child (Child (RootOfTree(1), 2), 2), 2), 1);
        AddTree (FalseNode, TempTree, 1);

        TempTree = Child (Child (Child (RootOfTree(1), 2), 2), 2);
        AddTree (IdentifierNode, TempTree, 2);
        TempTree = Child (Child (Child (Child (RootOfTree(1), 2), 2), 2), 2);
        AddTree (TrueNode, TempTree, 1);

        /* build char type node */
        TempTree = Child (Child (RootOfTree(1), 2), 3);
        AddTree (IdentifierNode, TempTree, 1);
        TempTree = Child (Child (Child (RootOfTree(1), 2), 3), 1);
        AddTree (CharacterTNode, TempTree, 1);

        /* initialize types */
        TypeInteger = Child (Child (RootOfTree(1), 2), 1);
        TypeBoolean = Child (Child (RootOfTree(1), 2), 2);
        TypeCharacter = Child (Child (RootOfTree(1), 2), 3);
}



void ErrorHeader (TreeNode T)
{
        printf ("<<< CONSTRAINER ERROR >>> AT ");
        Write_String (stdout,SourceLocation(T));
        printf (" : ");
        printf ("\n");
}

void WarningHeader (TreeNode T)
{
        printf ("<<< CONSTRAINER WARNING >>> AT ");
        Write_String (stdout,SourceLocation(T));
        printf (" : ");
        printf ("\n");
}

TreeNode GetIdentifierType(TreeNode Declaration)
{
        return NodeName(Decoration(Child(Declaration, 1)));
}

TreeNode Declaration(TreeNode T)
{
        return Lookup (NodeName(Child(T,1)), T);
}

unsigned char IsFresh(TreeNode T)
{
        return (Element(DclnTable->CurrentDcln, NodeName(Child(T,1))) == NullDeclaration);
}

UserType Expression (TreeNode T)
{
        if (TraceSpecified)
        {
                fprintf (TraceFile, "<<< CONSTRAINER >>> : Expn Processor Node ");
                Write_String (TraceFile, NodeName(T));
                fprintf (TraceFile, "\n");
        }


        switch (NodeName(T))
        {
        case LENode:
        case GENode:
        case GTNode:
        case LTNode:
                if (Expression (Child(T,1)) != TypeInteger || Expression (Child(T,2)) != TypeInteger)
                {
                        ErrorHeader(T);
                        printf ("ARGUMENTS OF '<=', '>=', '>', '<' MUST BE TYPE INTEGER\n");
                        printf ("\n");
                }
                return (TypeBoolean);


        case EqNode:
        case NEqNode:
                if (Expression (Child(T,1)) != Expression (Child(T,2)))
                {
                        ErrorHeader(T);
                        printf ("ARGUMENTS OF '=', '<>' MUST BE OF SAME TYPE\n");
                        printf ("\n");
                }
                return (TypeBoolean);


        case OrNode:
        case AndNode:
                if (Expression (Child(T,1)) != TypeBoolean || Expression (Child(T,2)) != TypeBoolean)
                {
                        ErrorHeader(T);
                        printf ("ARGUMENTS OF 'or', 'and' MUST BE TYPE BOOLEAN\n");
                        printf ("\n");
                }
                return (TypeBoolean);


        case PlusNode:
        case StarNode:
        case DivNode:
        case ModNode:
        case ExpNode:
                if (Expression (Child(T,1)) != TypeInteger || Expression (Child(T,2)) != TypeInteger)
                {
                        ErrorHeader(T);
                        printf ("ARGUMENTS OF '+', '*', '/', mod, '**' ");
                        printf ("MUST BE TYPE INTEGER\n");
                        printf ("\n");
                }
                return (TypeInteger);

        case MinusNode:
                if (Expression (Child(T,1)) != TypeInteger || (Rank(T)==2?Expression (Child(T,2)):TypeInteger) != TypeInteger)
                {
                        ErrorHeader(T);
                        printf ("ARGUMENTS OF '+', '-' ");
                        printf ("MUST BE TYPE INTEGER\n");
                        printf ("\n");
                }
                return (TypeInteger);


        case NotNode:
                if (Expression (Child(T,1)) != TypeBoolean)
                {
                        ErrorHeader(T);
                        printf ("ARGUMENT OF 'not' MUST BE BOOLEAN\n");
                        printf ("\n");
                }
                return (TypeBoolean);

        case EOFNode:
                return (TypeBoolean);

        case SuccNode:
        case PredNode:{
                UserType Type = Expression(Child(T, 1));
                Decorate(T, Type);
                return Type;
                }

        case OrdNode:
                Expression(Child(T, 1));
                return TypeInteger;


        case ChrNode:
                if (Expression (Child(T,1)) != TypeInteger){
                        ErrorHeader(T);
                        printf ("ARGUMENT OF CHR MUST BE INTEGER\n");
                        printf ("\n");
                }
                Decorate(T, TypeCharacter);
                return TypeCharacter;

        case IntegerNode:
                return (TypeInteger);

        case CharacterNode:
                Decorate(T, TypeCharacter);
                return (TypeCharacter);

        case IdentifierNode:
                if (IsFresh(T)){ /* identifier should be already declared */
                        ErrorHeader(T);
                        printf ("UNDECLARED IDENTIFIER\n");
                        printf ("\n");
                        return (TypeInteger);
                }
                else if (GetIdentifierType(Declaration(T)) == TypeNode){ /* identifier cannot be a type identifier */
                        ErrorHeader(T);
                        printf ("TYPE NAME SHOULD NOT BE USED AS LVALUE\n");
                        printf ("\n");
                        return (TypeInteger);
                }
                else if ((GetIdentifierType(Declaration(T)) == ProgramNode || GetIdentifierType(Declaration(T)) == ProcedureNode) && NodeName(Child(T, 1)) != NodeName(Child(Child(Lookup(SUBPROG_CTXT, T), 1), 1))){
                        ErrorHeader(T);
                        printf ("FUNCTION NAME SHOULD NOT BE USED AS LVALUE OR RVALUE OUTSIDE THE CORRESPONDING FUNCTION\n");
                        printf ("\n");
                        return (TypeInteger);
                }
                else {
                        Decorate (T, Declaration(T));
                        return (Decoration(Declaration(T)));
                }

        case CallNode:
                if (IsFresh(Child(T, 1))){ /* function should be already declared */
                        ErrorHeader(Child(T, 1));
                        printf ("UNDECLARED FUNCTION\n");
                        printf ("\n");
                        return (TypeInteger);
                } else if (GetIdentifierType(Declaration(Child(T, 1))) != FunctionNode){
                        ErrorHeader(Child(T, 1));
                        printf ("IDENTIFIER SHOULD BE OF TYPE FUNCTION\n");
                        printf ("\n");
                        return (TypeInteger);
                }

                {
                        Decorate(Child(T, 1), Declaration(Child(T, 1)));

                        TreeNode Temp = Child(Decoration(Child(Decoration(Child(T, 1)), 1)), 2); /* corresponding paramsnode */
                        int Params = 0;
                        int Kid;
                        for(Kid = 1; Kid <= Rank(Temp); Kid++)
                                Params += Rank(Child(Temp, Kid)) - 1;

                        if(Params != (Rank(T) - 1)){
                                ErrorHeader(Child(T, 1));
                                printf ("ARGUMENT COUNT DOES NOT MATCH DECLARATION\n");
                                printf ("\n");
                                return Decoration(Decoration(Child(T, 1)));
                        } else {
                                int DKid = 1;
                                int DKidKid = 1;
                                for(Params = 2; Params <= Rank(T); Params++){
                                        if(Expression(Child(T, Params)) != Decoration(Decoration(Child(Child(Temp, DKid), Rank(Child(Temp, DKid)))))){
                                                ErrorHeader(Child(T, Params));
                                                printf ("ARGUMENT TYPE DOES NOT MATCH DECLARATION TYPE\n");
                                                printf ("\n");
                                                return Decoration(Decoration(Child(T, 1)));
                                        }
                                        DKidKid++;
                                        if(DKidKid >= Rank(Child(Temp, DKid))){
                                                DKid++;
                                                DKidKid = 1;
                                        }
                                }
                                return Decoration(Decoration(Child(T, 1)));
                        }
                }

        default:
                ErrorHeader(T);
                printf ( "UNKNOWN NODE NAME ");
                Write_String (stdout,NodeName(T));
                printf ("\n");

        } /* end switch */
}  /* end Expression */


void ProcessNode (TreeNode T)
{
        int Kid;

        if (TraceSpecified)
        {
                fprintf (TraceFile,
                         "<<< CONSTRAINER >>> : Stmt Processor Node ");
                Write_String (TraceFile, NodeName(T));
                fprintf (TraceFile, "\n");
        }

        switch (NodeName(T))
        {
        case ProgramNode:
                if (NodeName(Child(Child(T,1),1)) != NodeName(Child(Child(T,Rank(T)),1)))
                {
                        ErrorHeader(T);
                        printf ("PROGRAM NAMES DO NOT MATCH\n");
                        printf ("\n");
                }

                OpenScope();

                ProcessNode(Child(T, 2));

                OpenScope();
                DTEnter(LOOP_CTXT, T, T);
                DTEnter(FOR_CTXT, T);
                DTEnter(SUBPROG_CTXT, T);

                for (Kid = 3; Kid <= Rank(T)-1; Kid++)
                        ProcessNode (Child(T,Kid));

                CloseScope();
                CloseScope();

                break;

        case SubProgsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;

        case FunctionNode:
                if (NodeName(Child(Child(T,1),1)) != NodeName(Child(Child(T,Rank(T)),1)))
                {
                        ErrorHeader(T);
                        printf ("FUNCTION NAMES DO NOT MATCH\n");
                        printf ("\n");
                }
                DTEnter(NodeName(Child(Child(T,1),1)), Child(T, 1));

                OpenScope();
                DTEnter(SUBPROG_CTXT, T);
                Decorate(Child(Child(T, 1), 1), T);

                ProcessNode(Child(T, 2));
                Decorate(Child(T, 1), Decoration(Lookup(NodeName(Child(Child(T, 3), 1)), T)));
                for (Kid = 4; Kid < Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));

                CloseScope();
                break;

        case ProcedureNode:
                if (NodeName(Child(Child(T,1),1)) != NodeName(Child(Child(T,Rank(T)),1)))
                {
                        ErrorHeader(T);
                        printf ("FUNCTION NAMES DO NOT MATCH\n");
                        printf ("\n");
                }
                DTEnter(NodeName(Child(Child(T,1),1)), Child(T, 1));

                OpenScope();
                DTEnter(SUBPROG_CTXT, T);
                Decorate(Child(Child(T, 1), 1), T);

                for (Kid = 2; Kid < Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));

                CloseScope();
                break;

        case ParamsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;

        case ReturnNode:{
                        TreeNode Ctxt = Lookup(SUBPROG_CTXT, T);
                        if(NodeName(Ctxt) != FunctionNode){
                                ErrorHeader(T);
                                printf ("ONLY FUNCTIONS SHOULD RETURN VARIABLES\n");
                                printf ("\n");
                        } else if (Rank(T) && Decoration(Child(Ctxt, 1)) != Expression(Child(T, 1))){
                                ErrorHeader(T);
                                printf ("RETURN DATA TYPE SHOULD MATCH THE FUNCTION RETURN TYPE\n");
                                printf ("\n");
                        }
                }break;

        case CallNode:
                if (IsFresh(Child(T, 1))){ /* function should be already declared */
                        ErrorHeader(Child(T, 1));
                        printf ("UNDECLARED PROCEDURE\n");
                        printf ("\n");
                } else if (GetIdentifierType(Declaration(Child(T, 1))) != ProcedureNode){
                        ErrorHeader(Child(T, 1));
                        printf ("IDENTIFIER SHOULD BE OF TYPE PROCEDURE\n");
                        printf ("\n");
                }

                {
                        Decorate(Child(T, 1), Declaration(Child(T, 1)));

                        TreeNode Temp = Child(Decoration(Child(Decoration(Child(T, 1)), 1)), 2); /* corresponding paramsnode */
                        int Params = 0;
                        int Kid;
                        for(Kid = 1; Kid <= Rank(Temp); Kid++)
                                Params += Rank(Child(Temp, Kid)) - 1;

                        if(Params != (Rank(T) - 1)){
                                ErrorHeader(Child(T, 1));
                                printf ("ARGUMENT COUNT DOES NOT MATCH DECLARATION\n");
                                printf ("\n");
                        } else {
                                int DKid = 1;
                                int DKidKid = 1;
                                for(Params = 2; Params <= Rank(T); Params++){
                                        if(Expression(Child(T, Params)) != Decoration(Decoration(Child(Child(Temp, DKid), Rank(Child(Temp, DKid)))))){
                                                ErrorHeader(Child(T, Params));
                                                printf ("ARGUMENT TYPE DOES NOT MATCH DECLARATION TYPE\n");
                                                printf ("\n");
                                        }
                                        DKidKid++;
                                        if(DKidKid >= Rank(Child(Temp, DKid))){
                                                DKid++;
                                                DKidKid = 1;
                                        }
                                }
                        }
                }
                break;


        case TypesNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;

        case TypeNode:
                Decorate(Child(Child(T, 1), 1),  T);
                DTEnter(NodeName(Child(Child(T, 1), 1)), Child(T, 1));
                if(Rank(T) == 1) {
                        Decorate(Child(T, 1),  T);
                }
                else if(!IsFresh(Child(T, 1)) && GetIdentifierType(Child(T, 1)) != TypeNode){ /* only type identifiers can be redefined */
                        ErrorHeader(T);
                        printf ("INVALID IDENTIFIER BEING REDEFINED\n");
                        printf ("\n");
                }
                else if(NodeName(Child(T, 2)) == IdentifierNode){
                        if(IsFresh(Child(T, 2)) || GetIdentifierType(Child(T, 2)) != TypeNode) /* identifier should be an already declared type */
                        {
                                ErrorHeader(T);
                                printf ("RHS IDENTIFIER SHOULD ALREADY BE DEFINED\n");
                                printf ("\n");
                        }
                        else
                        {
                                Decorate(Child(T, 2), Lookup(NodeName(Child(Child(T, 2), 1)), Child(T, 2)));
                                Decorate(Child(T, 1), Decoration(Decoration(Child(T, 2))));
                        }
                }
                else if(NodeName(Child(T, 2)) == LitNode){
                        Decorate(Child(T, 1),  T);
                        for(Kid = 1; Kid <= Rank(Child(T, 2)); Kid++){
                                if(IsLocal(Child(Child(T, 2), Kid))) /* identifier should a fresh name. redeclaration of enum will result in ambiguities. */
                                {
                                        ErrorHeader(T);
                                        printf ("IDENTIFIER SHOULD NOT BE REUSED\n");
                                        printf ("\n");
                                }
                                else
                                {
                                        DTEnter(NodeName(Child(Child(Child(T, 2), Kid), 1)), Child(Child(T, 2), Kid));
                                        Decorate(Child(Child(T, 2), Kid), T);
                                        Decorate(Child(Child(Child(T, 2), Kid), 1), Child(T, 2));
                                }
                        }
                }
                break;

        case ConstsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;

        case ConstNode:
                if(!IsFresh(Child(T, 1))) /* should not allow redeclaration of identifiers */
                {
                        ErrorHeader(T);
                        printf ("REDECLARATION OF IDENTIFIER\n");
                        printf ("\n");
                }
                else if (NodeName(Child(T, 2)) == IdentifierNode && (GetIdentifierType(Declaration(Child(T, 2))) == DclnNode || IsFresh(Child(T, 2)))) /* invalidate Dcln. type is invalidated by expression */
                {
                        ErrorHeader(T);
                        printf ("INVALID IDENTIFIER USED AS ARGUMENT\n");
                        printf ("\n");
                }
                else
                {
                        Decorate(Child(Child(T, 1), 1),  T);
                        DTEnter(NodeName(Child(Child(T, 1), 1)), Child(T, 1), T);
                        Decorate(Child(T, 1), Expression(Child(T, 2)));
                }
                break;

        case DclnsNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;


        case DclnNode:
                if(IsFresh(Child(T, Rank(T))) || GetIdentifierType(Declaration(Child(T, Rank(T)))) != TypeNode) /* Type name has to be a declared type! */
                {
                        ErrorHeader(Child(T, Rank(T)));
                        printf ("IDENTIFIER HAS TO BE A DECLARED TYPE\n");
                        printf ("\n");
                }
                else
                {
                        Decorate(Child(T, Rank(T)), Declaration(Child(T, Rank(T))));
                        for (Kid  = 1; Kid < Rank(T); Kid++)
                        {
                                if(IsLocal(Child(T, Kid))){ /* variable name has to be a fresh name */
                                        ErrorHeader(T);
                                        printf ("REDECLARATION OF VARIABLES NOT ALLOWED\n");
                                        printf ("\n");
                                }
                                else
                                {
                                        Decorate(Child(Child(T, Kid), 1),  T);
                                        DTEnter(NodeName(Child(Child(T,Kid),1)), Child(T,Kid), T);
                                        Decorate (Child(T,Kid), Decoration(Declaration(Child(T, Rank(T)))));
                                }
                        }
                }
                break;


        case BlockNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                break;

        case AssignNode:
                if(!(
                    (GetIdentifierType(Declaration(Child(T, 1))) == ProcedureNode || GetIdentifierType(Declaration(Child(T, 1))) == FunctionNode)
                        && Decoration(Child(Declaration(Child(T, 1)), 1)) == Lookup(SUBPROG_CTXT, T)
                    ) &&  GetIdentifierType(Declaration(Child(T, 1))) != DclnNode){
                        ErrorHeader(T);
                        printf ("LHS HAS TO BE VARIABLE\n");
                        printf ("\n");
                }
                if (Expression (Child(T,1)) != Expression (Child(T,2))){
                        ErrorHeader(T);
                        printf ("ASSIGNMENT TYPES DO NOT MATCH\n");
                        printf ("\n");
                }
                {
                        TreeNode Temp = Lookup(FOR_CTXT, T);
                        while(NodeName(Temp) != ProgramNode) {
                                if(NodeName(Child(Child(Temp, 1), 1)) == NodeName(Child(Child(T, 1), 1))) {
                                        ErrorHeader(T);
                                        printf ("CANNOT MODIFY FOR LOOP VARIABLE\n");
                                        printf ("\n");
                                }
                                Temp = Decoration(Temp);
                        }
                }
                break;

        case SwapNode:
                if(GetIdentifierType(Declaration(Child(T, 1))) != DclnNode || GetIdentifierType(Declaration(Child(T, 2))) != DclnNode){
                        ErrorHeader(T);
                        printf ("BOTH LHS AND RHS HAVE TO BE VARIABLES\n");
                        printf ("\n");
                }
                if (Expression (Child(T,1)) != Expression (Child(T,2)))
                {
                        ErrorHeader(T);
                        printf ("SWAP TYPES DO NOT MATCH\n");
                        printf ("\n");
                }
                {
                        TreeNode Temp = Lookup(FOR_CTXT, T);
                        while(NodeName(Temp) != ProgramNode) {
                                if(NodeName(Child(Child(Temp, 1), 1)) == NodeName(Child(Child(T, 1), 1)) || NodeName(Child(Child(Temp, 1), 1)) == NodeName(Child(Child(T, 2), 1))) {
                                        ErrorHeader(T);
                                        printf ("CANNOT MODIFY FOR LOOP VARIABLE\n");
                                        printf ("\n");
                                }
                                Temp = Decoration(Temp);
                        }
                }
                break;


        case OutputNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        if (NodeName(Child(T, Kid)) != StringNode){
                            UserType Type = Expression(Child(T,Kid));
                            if (Type != TypeInteger && Type != TypeCharacter)
                            {
                                    ErrorHeader(T);
                                    printf ("OUTPUT EXPRESSION MUST BE INTEGER, CHARACTER OR STRING\n");
                                    printf ("\n");
                            }
                        }
                break;

        case ReadNode:
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        if (GetIdentifierType(Declaration(Child(T, Kid))) != DclnNode || (Expression (Child(T,Kid)) != TypeInteger && Expression(Child(T,Kid)) != TypeCharacter)) /* identifier should be variable and either integer or character */
                        {
                                ErrorHeader(T);
                                printf ("INPUT VARIABLE SHOULD BE OF TYPE CHARACTER OR INTEGER\n");
                                printf ("\n");
                        }
                        else if (NodeName(Lookup(FOR_CTXT, Child(T, Kid))) != ProgramNode && Declaration(Child(Lookup(FOR_CTXT, Child(T, Kid)), 1)) == Declaration(Child(T, Kid)))
                        {
                                ErrorHeader(T);
                                printf ("FOR LOOP CONTROL VARIABLE CANNOT BE MODIFIED\n");
                                printf ("\n");
                        }
                break;


        case IfNode:
                if (Expression (Child(T,1)) != TypeBoolean)
                {
                        ErrorHeader(T);
                        printf ("CONTROL EXPRESSION OF 'IF' STMT IS NOT TYPE BOOLEAN\n");
                        printf ("\n");
                }

                ProcessNode (Child(T,2));
                if (Rank(T) == 3)
                        ProcessNode (Child(T,3));
                break;


        case WhileNode:
                if (Expression (Child(T,1)) != TypeBoolean)
                {
                        ErrorHeader(T);
                        printf ("WHILE EXPRESSION NOT OF TYPE BOOLEAN\n");
                        printf ("\n");
                }
                ProcessNode (Child(T,2));
                break;

        case RepeatNode:
                for (Kid = 1; Kid <= Rank(T) - 1; Kid++)
                        ProcessNode (Child(T,Kid));
                if (Expression (Child(T,Rank(T))) != TypeBoolean)
                {
                        ErrorHeader(T);
                        printf ("REPEAT EXPRESSION NOT OF TYPE BOOLEAN\n");
                        printf ("\n");
                }
                break;

        case LoopNode:
                OpenScope();
                DTEnter(LOOP_CTXT, T, T);
                for (Kid = 1; Kid <= Rank(T); Kid++)
                        ProcessNode (Child(T,Kid));
                CloseScope();
                if(Decoration(T) == 0)
                {
                        WarningHeader(T);
                        printf ("LOOP DOES NOT CONTAIN EXIT STATEMENT\n");
                        printf ("\n");
                }
                break;

        case ExitNode:{
                        TreeNode Temp = Lookup(LOOP_CTXT, T);
                        if(NodeName(Temp) != LoopNode)
                        {
                                ErrorHeader(T);
                                printf ("EXIT STATEMENT APPEARS OUTSIDE A LOOP\n");
                                printf ("\n");
                        }
                        Decorate(T, Temp);
                        Decorate(Temp, T);
                }
                break;

        case CaseNode:
                Decorate(T, Expression(Child(T, 1)));
                for (Kid = 2; Kid <= Rank(T); Kid++){
                        Decorate(Child(T, Kid), T);
                        ProcessNode (Child(T, Kid));
                }
                break;

        case CaseItemNode:
                if(NodeName(Child(T, 1)) == DDNode){
                    if(Expression(Child(Child(T, 1), 1)) != Decoration(Decoration(T)) || Expression(Child(Child(T, 1), 2)) != Decoration(Decoration(T)))
                    {
                            ErrorHeader(Child(T, 1));
                            printf ("CASE CRITERION TYPE SHOULD MATCH CASE VARIABLE TYPE\n");
                            printf ("\n");
                    }
                }
                else
                    if(Expression(Child(T, 1)) != Decoration(Decoration(T)))
                    {
                            ErrorHeader(Child(T, 1));
                            printf ("CASE CRITERION TYPE SHOULD MATCH CASE VARIABLE TYPE\n");
                            printf ("\n");
                    }
                ProcessNode(Child(T, 2));
                break;

        case OtherwiseNode:
                ProcessNode(Child(T, 1));
                break;

        case UptoNode:
        case DowntoNode:{
                        TreeNode Temp = Lookup(FOR_CTXT, T);
                        Decorate(T, Temp);
                        OpenScope();
                        DTEnter(FOR_CTXT, T);
                        DTEnter(LOOP_CTXT);
                        UserType Type = Expression (Child(T,1));
                        if(Type != Expression (Child(T,2)) || Type != Expression (Child(T,3)))
                        {
                                ErrorHeader(T);
                                printf ("FOR EXPRESSION NOT VALID\n");
                                printf ("\n");
                        }
                        ProcessNode (Child(T,4));
                        while(NodeName(Temp) != ProgramNode) {
                                if(NodeName(Child(Child(Temp, 1), 1)) == NodeName(Child(Child(T, 1), 1))) {
                                        ErrorHeader(T);
                                        printf ("CANNOT MODIFY FOR LOOP VARIABLE\n");
                                        printf ("\n");
                                }
                                Temp = Decoration(Temp);
                        }
                        CloseScope();
                }
                break;

        case NullNode:
                break;

        case DDNode:
        case StringNode:
        case LitNode:
        default:
                ErrorHeader(T);
                printf ("UNKNOWN NODE NAME ");
                Write_String (stdout,NodeName(T));
                printf ("\n");

        } /* end switch */
}  /* end ProcessNode */
